<?php

/*

Class Name: MetaBox
Author Name: J. Rafid S.
Author URI: www.google.com
Description: Accepts a 2D array of Fields and creates postmeta box with the given fields.

$fields = array(
     
array(

    "name" => "field name",
     "id" => "field id",
     "type" => "display type"
     
    )
);

*/

class MetaBox
{
    private $_fields;
    private $_metabox_id;
    private $_metabox_name;
    private $_is_mutually_exclusive;
    private $_meta_key;
    private $_meta_prev_value;
    private $_meta_new_value;
    private $_new_post_type;

    public function __construct($metabox_id, $new_post_type="" ,$metabox_name, $fields,$is_mutually_exclusive=false,$meta_key="",$meta_prev_value="yes",$meta_new_value="no")
    {

        $this->_fields = $fields;
        $this->_metabox_id = $metabox_id;
        $this->_metabox_name = $metabox_name;
        $this->_is_mutually_exclusive = $is_mutually_exclusive;
        $this->_meta_key = $meta_key;
        $this->_meta_prev_value = $meta_prev_value;
        $this->_meta_new_value = $meta_new_value;
        $this->_new_post_type = $new_post_type;

    if ($new_post_type[0] != "") {
      if ($new_post_type[0] == "yes") {
        add_action('init', array($this, 'create_posttype'));
        add_action("add_meta_boxes_$new_post_type[1]", array($this, 'add_post_meta_box'));
      }
    } else
      add_action("add_meta_boxes", array($this, 'add_post_meta_box'));
        add_action('save_post', array($this,'save_metabox_data'), 10, 2);

    }

  //Create Custom posttype
  function create_posttype()
  {
    register_post_type($this->_new_post_type[1], array(
      
      'labels' => array(
        'name' => $this->_new_post_type[2],
        'singular_name' => $this->_new_post_type[1],
        'add_new' => 'Add new '.$this->_new_post_type[1],
        'add_new_item' => 'Add new '.$this->_new_post_type[1],
        'all_items' => 'All '.$this->_new_post_type[2],
      ),
      'supports' => array('title', 'editor', 'thumbnail'),
      'public' => true,
      'show_in_menu' => true,
      'rewrite' => array('slug' => $this->_new_post_type[1]),
      'register_meta_box_cb' => array($this,'add_post_meta_box')
    ));
  }

  


    // Add a metabox for each post
    public function add_post_meta_box($post)
    {
      if($this->_new_post_type != "")
          add_meta_box($this->_metabox_id, __($this->_metabox_name, $this->_metabox_id), array($this,'metabox_callback'), $this->_new_post_type[1], 'side', 'low');
        else
          add_meta_box($this->_metabox_id, __($this->_metabox_name, $this->_metabox_id), array($this,'metabox_callback'), 'post', 'side', 'low');
    }

    // Metabox callback function
    public function metabox_callback($post)
    {
        
    wp_nonce_field(basename(__FILE__), 'meta_box_nonce');  //Check to see if request is from a valid page
    
    printf('<div class="inside">');
    
    // Adding fields
    for($i=0;$i<count($this->_fields);$i++)
    {
        
        $field_name = $this->_fields[$i]['name'];
        $field_id = $this->_fields[$i]['id'];
        $field_type = $this->_fields[$i]['type'];
        
        $current_value = get_post_meta($post->ID, $field_id, true);

        if($field_type == "checkbox")
                printf('<input name="%1$s" id="%1$s" type="%2$s" placeholder="%3$s" value="yes" class=""'. checked("yes", $current_value,false) . ' /> %4$s <br>', $field_id, $field_type,$current_value,$field_name);
            else
                printf('%1$s: <input name="%2$s" id="%2$s" type="%3$s" placeholder="%4$s" value="%4$s" class="" /> <br>',$field_name, $field_id, $field_type,$current_value);
    }
    printf('</div>');

    }



// Stores the meta box values
public function save_metabox_data($post_id)
{
  
  if($this->_is_mutually_exclusive)
  {
    $this->reset_post_meta($this->_meta_key,$this->_meta_prev_value,$this->_meta_new_value);
  }

  if (!isset($_POST['meta_box_nonce']) || !wp_verify_nonce($_POST['meta_box_nonce'], basename(__FILE__))) {
    return;
  }

  if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
    return;
  }
  if (!current_user_can('edit_post', $post_id)) {
    return;
  }
  
    for($i=0;$i<count($this->_fields);$i++)
    {
        
        $field_name = $this->_fields[$i]['name'];
        $field_id = $this->_fields[$i]['id'];
        $field_type = $this->_fields[$i]['type'];
        
        update_post_meta($post_id, $field_id, sanitize_text_field($_POST[$field_id]));
        
    }
  
}


public function reset_post_meta($meta_key,$meta_prev_value,$meta_new_value)
{
  
  $args = array(
    'meta_key' => $meta_key,
    'meta_value' => $meta_prev_value,
  );
  $query = new WP_Query($args);

  if ($query->have_posts()) {
    while ($query->have_posts()) {
      $query->the_post();
      update_post_meta(get_the_id(), $meta_key, $meta_new_value);
    }
    wp_reset_postdata();
  }

}

}


?>


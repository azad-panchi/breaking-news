<?php

/*

Plugin Name: Breaking_News
Plugin URI: www.google.com
Author Name: J. Rafid S.
Author URI: www.google.com
Version: 1.0

*/
include("settings.php");
include("metabox.php");

define('PLUGIN_NAME', 'Breaking News');
define('PLUGIN_SLUG', 'breaking-news');

define('PLUGIN_DIR', plugin_dir_path(__FILE__));
define('PLUGIN_URL', plugin_dir_url(__FILE__));


function plugin_resources()
{
    wp_enqueue_style("style",PLUGIN_URL.'/style.css');
}

add_action("wp_enqueue_scripts","plugin_resources");

////////////////////// ADD Breaking News Post Metabox   /////////////////////////

if(is_admin())
{


$fields = array(
  
  array(
  
    "name" => "Is Breaking News",
     "id" => "is_breaking_news",
     "type" => "checkbox"
     
  ),   
  array(
  
      "name" => "Title",
       "id" => "breaking_news_title",
       "type" => "text"
       
  ),
  array(
  
    "name" => "Is Expirable",
     "id" => "is_expirable",
     "type" => "checkbox"
     
  ),

  array(
  
    "name" => "Expiry Date",
     "id" => "expiry_date",
     "type" => "date"
     
  )
  );

  $postmeta  = new MetaBox(PLUGIN_SLUG,array("yes","movie","Movies"),'Breaking News Options',$fields,true,"is_breaking_news","yes","no");

 
/////////////////////  Options Page of Plugin ///////////////////////////

$fields = array(
     
  array(
  
      "name" => "Breaking News Title",
       "id" => "breaking_news_title",
       "section" => "section1",
       "type" => "text"
       
  ),
  array(
  
    "name" => "Background Color",
     "id" => "breaking_news_background_color",
     "section" => "section1",
     "type" => "color"
     
  ),

  array(
  
    "name" => "Text Color",
     "id" => "breaking_news_text_color",
     "section" => "section1",
     "type" => "color"
     
  ),

  array(
  
    "name" => "Use Shortcode",
     "id" => "use_shortcode",
     "section" => "section1",
     "type" => "checkbox"
     
  )

  );
  

  $sections = array(
       
      "section1" => "Basic Settings",
       "section2" => "Advanced Settings"
  
  );
  

$settings = new Settings($fields,$sections,PLUGIN_NAME." Options",PLUGIN_NAME,"manage_options",PLUGIN_SLUG);

}

//////////////  Display the Breaking news post (Front-end) /////////////////////

function breaking_news_display_callback($post)
{

  $args = array(
    'meta_key' => 'is_breaking_news',
    'meta_value' => 'yes'    
  );
  $query = new WP_Query($args);

  if ($query->have_posts()) {
      $query->the_post();
      $now =  new DateTime('now');
      $post_datetime = new DateTime(get_post_meta(get_the_id(),'expiry_date',true));
      if($now<=$post_datetime) {
        $background_color = get_option('breaking_news_background_color');
        $text_color = get_option('breaking_news_text_color');
        echo('<a  href="'.get_permalink().'" style="color:'.$text_color.';background-color:'.$background_color.'" >Breaking News: '.get_the_title().'</a>');
      }

    wp_reset_postdata();
  }

}

//add_action('breaking_news_hook','breaking_news_display_callback');
add_action('init','breaking_news_display_callback');

?>
<?php

/*

Class Name: Settings
Author Name: J. Rafid S.
Author URI: www.google.com
Description: Accepts a 2D array of Fields and corresponding sections as arguments and creates admin section settings for the plugin.

$fields = array(
     
array(

    "name" => "setting name",
     "id" => "setting id",
     "section" => "section name",
     "type" => "display type", ...
     
    )
);

$sections = array(
     
    "section1" => "section1 heading",
     "section2" => "section2 heading"

);


*/


class Settings
{
    private $_fields = array();
    private $_sections = array();
    private $_page_title;
    private $_menu_title;
    private $_capability;
    private $_slug;
    
    public function __construct($fields,$sections,$page_title,$menu_title,$capability,$slug="slug")
    {
        $this->_fields = $fields;
        $this->_sections = $sections;
        $this->_page_title = $page_title;
        $this->_menu_title = $menu_title;
        $this->_capability = $capability;
        $this->_slug = $slug;

        add_action('admin_menu', array($this, 'create_settings'));
        add_action('admin_init', array($this, 'setup_sections'));
        add_action('admin_init', array($this, 'setup_fields'));
    }
    
    public function create_settings()
    {
        add_options_page($this->_page_title, $this->_menu_title, $this->_capability, $this->_slug, array($this, 'page_callback'));        
    }

    public function page_callback()
    {
        ?>
        <div class="wrap">
            <h1><?php echo $this->_page_title?></h1>
            <?php settings_errors(); ?>
            <form method="POST" action="options.php">
                <?php
                settings_fields($this->_slug);
                do_settings_sections($this->_slug);
                submit_button();
                ?>
            </form>
        </div>
        <?php
    }


    public function setup_sections()
    {
        
        for($i=0;$i<count($this->_sections);$i++)
        {
            add_settings_section(array_keys($this->_sections)[$i], array_values($this->_sections)[$i], array(), $this->_slug);
        }
    }

    public function setup_fields()
    {
        for($i=0;$i<count($this->_fields);$i++)
        {

            $current_value = get_option($this->_fields[$i]['id']);
            $field_name = $this->_fields[$i]['name'];
            $field_id = $this->_fields[$i]['id'];
            $field_type = $this->_fields[$i]['type'];
            $section_name = $this->_fields[$i]['section'];

            add_settings_field($field_id, $field_name, array($this,'field_callback'), $this->_slug, $section_name, array($field_id, $field_type, $current_value));
            register_setting($this->_slug, $field_id);

        }

        
    }

    public function field_callback($args)
    {
        $field_id = $args[0];
        $field_type = $args[1];
        $current_value = $args[2];
        
        

        if($field_type == "checkbox")
            printf('<input name="%1$s" id="%1$s" type="%2$s" placeholder="%3$s" value="%1$s" class=""'. checked($field_id, $current_value,false) . ' />', $field_id, $field_type,$current_value);
        else
            printf('<input name="%1$s" id="%1$s" type="%2$s" placeholder="%3$s" value="%3$s" class="" />', $field_id, $field_type,$current_value);

    }

}

?>